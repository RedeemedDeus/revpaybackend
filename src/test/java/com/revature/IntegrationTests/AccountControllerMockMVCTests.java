package com.revature.IntegrationTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.util.ArrayList;
import java.util.List;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.revature.Controller.AccountController;
import com.revature.Model.Account;
import com.revature.Repository.AccountRepo;
import com.revature.Service.AccountService;

@WebMvcTest
public class AccountControllerMockMVCTests {
	
	@MockBean
	private AccountService accountService;
	
	@Autowired
	private MockMvc mockMvc;
	

	@Test
	public void givenData_whenGetAllAccounts_shouldReturnAllAccounts() throws Exception{
		List<Account> mockAccounts = new ArrayList<>();
		Account a1 = new Account((long)1,"johndoe","pass","john@email.com","123-111-1111",0.0);
		Account a2 = new Account((long)2,"janedoe","pass","jane@email.com","123-2222-2222",0.0);
		
		mockAccounts.add(a1);
		mockAccounts.add(a2);
		
		given(accountService.getAllLogin()).willReturn(mockAccounts);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/login");
		
		MvcResult mvcResult = mockMvc.perform(requestBuilder)
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].username").value("johndoe"))
				.andReturn();
		
		String response = mvcResult.getResponse().getContentAsString();
		
		List<Account> returnedAccounts = new ObjectMapper().readValue(response, List.class);
		
		assertEquals(2, returnedAccounts.size());
	}
	
	
	//POST TEST
	@Test
	public void givenData_whenRegisterAccount_shouldReturnAccount() throws Exception{
		
		this.mockMvc
		.perform(MockMvcRequestBuilders
				.post("/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"username\": \"johndoe\", \"password\": \"pass\", \"email\": \"john@email.com\", \"phoneNumber\": \"123-111-1111\"}")
				)
		.andDo(print())
		.andExpect(MockMvcResultMatchers.status().isCreated());
		
		
		verify(accountService).register(any(Account.class));
	}
	

}
