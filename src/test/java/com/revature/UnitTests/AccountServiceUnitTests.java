package com.revature.UnitTests;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.then;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.revature.Model.Account;
import com.revature.Repository.AccountRepo;
import com.revature.Service.AccountService;

@ExtendWith(MockitoExtension.class)
public class AccountServiceUnitTests {
	 
	@InjectMocks
	private AccountService accountService;
	
	@Mock
	private AccountRepo accountRepository;
	
	List<Account> mockAccounts = new ArrayList<>();
	
	@BeforeEach
	public void setup() {
		Account a1 = new Account((long)1,"johndoe","pass","john@email.com","123-111-1111",0.0);
		Account a2 = new Account((long)2,"janedoe","pass","jane@email.com","123-2222-2222",0.0);
		
		mockAccounts.add(a1);
		mockAccounts.add(a2);
	}
	
	@AfterEach
	public void teardown() {
		mockAccounts.clear();
	}
	
	@Test
	public void givenMockData_whenGetAllAccounts_thenReturnAllAccounts() {
		
		when(accountRepository.findAll()).thenReturn(mockAccounts);
		
		List<Account> returnedAccounts = accountService.getAllLogin();
		
		verify(accountRepository, times(1)).findAll();
		
		then(accountRepository).should().findAll();
		
		assertEquals(2, returnedAccounts.size());
	}
	
	@Test
	public void givenMockData_whenFindByEmail_thenReturnAccount() {
		when(accountRepository.findByEmail("john@email.com")).thenReturn(mockAccounts.get(0));
		
		Account returnedAccount = accountService.findByEmail("john@email.com");
		
		verify(accountRepository,times(1)).findByEmail("john@email.com");
		
		then(accountRepository).should().findByEmail("john@email.com");
		
		assertEquals(mockAccounts.get(0), returnedAccount);
	}

}
