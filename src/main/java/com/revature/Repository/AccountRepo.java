package com.revature.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.Model.Account;

public interface AccountRepo extends JpaRepository<Account,Long>{
	Account findByUsername(String username);
	
	Account findByEmail(String email);
	
	Account findByPhoneNumber(String phoneNumber);

}
