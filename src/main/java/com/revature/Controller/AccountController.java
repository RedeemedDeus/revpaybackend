package com.revature.Controller;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.revature.Exceptions.DuplicateUserException;
import com.revature.Exceptions.InsufficientFundsException;
import com.revature.Exceptions.UnauthorizedUserException;
import com.revature.Model.Account;
import com.revature.Service.AccountService;

@CrossOrigin(origins = {"*"})
@RestController
public class AccountController {
	AccountService accountService;
	
	@Autowired
	public AccountController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	/*
	 * 1. A user should be able to register themselves
	 * POST localhost:9000/register
	 * */
	@PostMapping("register")
	@ResponseStatus(HttpStatus.CREATED)
	public Account register(@RequestBody Account account) throws DuplicateUserException{
		String hashed = BCrypt.hashpw(account.getPassword(), BCrypt.gensalt(12));
		account.setPassword(hashed);
		
		return accountService.register(account);
	}
	
	/*
	 * 2. A user should be able to login to their account
	 * POST localhost:9000/login
	 * */
	@PostMapping("login")
	public Object login(@RequestBody Account account)throws UnauthorizedUserException{
		return accountService.login(account);
	}
	
	/*
	 * 2-1. A admin should be able to retrieve all login credentials
	 * GET localhost:9000/login
	 * */
	@GetMapping("login")
	public List<Account> getAllLogin(){
		return accountService.getAllLogin();
	}
	
	/*
	 * 3. A user should be able to deposit money to it's own account
	 * PATCH localhost:9000/account/{id}/deposit
	 * */
	@PatchMapping("account/{id}/deposit/{amount}")
	public Account deposit(@PathVariable long id, @PathVariable double amount) {
		return accountService.deposit(id, amount);
	}
	
	/*
	 * 4. A user should be able to send money using username, email, or phone number
	 * POST localhost:9000/account/{id}/send/{amount}
	 * */
	@PostMapping("account/{id}/send/{amount}")
	public Account sendMoney(@PathVariable long id, @PathVariable double amount, @RequestBody Account recipient) throws InsufficientFundsException {
		return accountService.sendMoney(id, amount, recipient);
	}
	
	/*
	 * 5. A user should be able to request money from other users using username, email, or phone number
	 * POST localhost:9000/account/{id}/request/{amount}
	 * */
	@PostMapping("account/{id}/request/{amount}")
	public Account requestMoney(@PathVariable long id, @PathVariable double amount, @RequestBody Account recipient) throws InsufficientFundsException{
		return accountService.requestMoney(id, amount, recipient);
	}
	
	
	
	
	
	
}
