package com.revature.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.Exceptions.DuplicateUserException;
import com.revature.Exceptions.InsufficientFundsException;
import com.revature.Exceptions.UnauthorizedUserException;
import com.revature.Model.Account;
import com.revature.Repository.AccountRepo;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

@Service
public class AccountService {
	AccountRepo accountRepository;
	
	@Autowired
	public AccountService(AccountRepo accountRepository) {
		this.accountRepository = accountRepository;
	}

	/**
	 * @param account
	 * @return Account saved in the repository
	 * @throws DuplicateUserException
	 */
	public Account register(Account account)throws DuplicateUserException {
		if(accountRepository.findByEmail(account.getEmail()) != null) {
			throw new DuplicateUserException();
		}
		
		return accountRepository.save(account);
	}

	/**
	 * 
	 * @param account
	 * @return accountOnDb after password matches
	 * @throws UnauthorizedUserException
	 */
	public Object login(Account account) throws UnauthorizedUserException{
		Account accountOnDb = accountRepository.findByEmail(account.getEmail());
		
		if(accountOnDb == null) {
			throw new UnauthorizedUserException();
		}
		
		if(BCrypt.checkpw(account.getPassword(), accountOnDb.getPassword())) {
			return accountOnDb;
		}
		else {
			throw new UnauthorizedUserException();
		}
	}

	/**
	 * @return List<Account> of all the accounts in the system
	 */
	public List<Account> getAllLogin() {
		return accountRepository.findAll();
	}

	/**
	 * 
	 * @param id
	 * @param amount
	 * @return accountOnDb once the amount has been deposited
	 */
	public Account deposit(long id, double amount) {
		Account accountOnDb = accountRepository.findById(id).get();
		accountOnDb.setBalance(accountOnDb.getBalance() + amount);
		
		return accountRepository.save(accountOnDb);
	}

	/**
	 * 
	 * @param id
	 * @param amount
	 * @param recipient
	 * @return senderAccount once the funds have been transfered
	 * @throws InsufficientFundsException
	 */
	public Account sendMoney(long id, double amount, Account recipient) throws InsufficientFundsException{
		
		Account senderAccount = accountRepository.findById(id).get();
		Account recipientByUsername = accountRepository.findByUsername(recipient.getUsername());
		Account recipientByEmail = accountRepository.findByEmail(recipient.getEmail());
		Account recipientByPhoneNumber = accountRepository.findByPhoneNumber(recipient.getPhoneNumber());
		
		if(recipientByUsername != null) {
			if(senderAccount.getBalance() >= amount) {
				senderAccount.setBalance(senderAccount.getBalance() - amount);
				recipientByUsername.setBalance(recipientByUsername.getBalance() + amount);
				
				accountRepository.save(recipientByUsername);
				return accountRepository.save(senderAccount);
			}
			
			return null;
		}
		else if(recipientByEmail != null) {
			if(senderAccount.getBalance() >= amount) {
				senderAccount.setBalance(senderAccount.getBalance() - amount);
				recipientByEmail.setBalance(recipientByEmail.getBalance() + amount);
				
				accountRepository.save(recipientByEmail);
				return accountRepository.save(senderAccount);
			}
			
			return null;
		}
		else if(recipientByPhoneNumber != null) {
			if(senderAccount.getBalance() >= amount) {
				senderAccount.setBalance(senderAccount.getBalance() - amount);
				recipientByPhoneNumber.setBalance(recipientByPhoneNumber.getBalance() + amount);
				
				accountRepository.save(recipientByPhoneNumber);
				return accountRepository.save(senderAccount);
			}
			
			return null;
		}
		
		return null;
	}

	/**
	 * 
	 * @param id
	 * @param amount
	 * @param recipient
	 * @return requesterAccount once the money requested has been transfered
	 */
	public Account requestMoney(long id, double amount, Account recipient) {
		
		Account requesterAccount = accountRepository.findById(id).get();
		Account recipientByUsername = accountRepository.findByUsername(recipient.getUsername());
		Account recipientByEmail = accountRepository.findByEmail(recipient.getEmail());
		Account recipientByPhoneNumber = accountRepository.findByPhoneNumber(recipient.getPhoneNumber());
		
		if(recipientByUsername != null) {
			if(recipientByUsername.getBalance() >= amount) {
				requesterAccount.setBalance(requesterAccount.getBalance() + amount);
				recipientByUsername.setBalance(recipientByUsername.getBalance() - amount);
				
				accountRepository.save(recipientByUsername);
				return accountRepository.save(requesterAccount);
			}
			
			return null;
		}
		else if(recipientByEmail != null) {
			if(recipientByEmail.getBalance() >= amount) {
				requesterAccount.setBalance(requesterAccount.getBalance() + amount);
				recipientByEmail.setBalance(recipientByEmail.getBalance() - amount);
				
				accountRepository.save(recipientByEmail);
				return accountRepository.save(requesterAccount);
			}
			
			return null;
		}
		else if(recipientByPhoneNumber != null) {
			if(recipientByPhoneNumber.getBalance() >= amount) {
				requesterAccount.setBalance(requesterAccount.getBalance() + amount);
				recipientByPhoneNumber.setBalance(recipientByPhoneNumber.getBalance() - amount);
				
				accountRepository.save(recipientByPhoneNumber);
				return accountRepository.save(requesterAccount);
			}
			
			return null;
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param email
	 * @return accountOnDb if an account matches the email;
	 */
	public Account findByEmail(String email) {
		Account accountOnDb = accountRepository.findByEmail(email);
		
		if(accountOnDb != null) {
			return accountOnDb;
		}
		
		return null;
	}
	
	
	
	
}
