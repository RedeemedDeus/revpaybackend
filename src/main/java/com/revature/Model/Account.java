package com.revature.Model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Account {
	
	@Id
	@Column(name = "account_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column(name = "account_username")
	private String username = "";
	
	@Column(name = "account_password")
	private String password = "";
	
	@Column(name = "account_email")
	private String email = "";
	
	@Column(name = "account_phone_number")
	private String phoneNumber = "000-000-0000";
	
	@Column(name = "account_balance")
	private double balance = 0.0;

}
